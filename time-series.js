conn = new Mongo();
db = conn.getDB("bootcamp");
colls = db.persample
collh = db.perhour
colld = db.perday

//-----------------------------------------------------------------------------
// Document per sample (dps)
//

function DocumentPerSample() {

    this.store = function( server_name, cpu_measurement, timestamp ) {
        colls.insert({
            load: cpu_measurement,
            server: server_name,
            timestamp: timestamp
        });
    };

    this.query = function( server_name, start, end ) {
        return colls.find({
            server: server_name,
            timestamp: {
                $gte: start,
                $lt: end
            }
        }).itcount();
    };

}


//-----------------------------------------------------------------------------
// Document per hour (dph)
//

function DocumentPerHour() {

    this.store = function( server_name, cpu_measurement, timestamp ) {
        timestamp.setMinutes(0);
        timestamp.setSeconds(0);
        collh.update(
            {
                server: server_name,
                hour: timestamp
            },
            {
                $inc: {
                    load_sum: cpu_measurement,
                    load_count: 1
                }
            },
            { upsert: true }
        );
    };

    this.query = function( server_name, start, end ) {
        start.setMinutes(0);
        start.setSeconds(0);
        return collh.find({
            server: server_name,
            hour: {
                $gte: start,
                $lt: end
            }
        }).itcount();
    };
}

//-----------------------------------------------------------------------------
// Document per day (dpd)
//

function DocumentPerDay() {

    this.store = function( server_name, cpu_measurement, timestamp ) {
        var h = timestamp.getUTCHours();
        timestamp.setMinutes(0);
        timestamp.setSeconds(0);
        timestamp.setUTCHours(0);
        inc = {};
        inc['hours.'+h+'.sum'] = cpu_measurement;
        inc['hours.'+h+'.count'] = 1;
        colld.update(
            { server: server_name, day: timestamp },
            { $inc: inc },
            { upsert: true }
        );
    };

    this.query = function( server_name, start, end ) {
        start.setMinutes(0);
        start.setSeconds(0);
        start.setUTCHours(0);
        return colld.find({
            server: server_name,
            day: {
                $gte: start,
                $lt: end
            }
        }).itcount();
    };
}
