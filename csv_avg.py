#! /usr/bin/env python

from os import listdir
from os.path import isfile, join, basename
import sys

def main():
    file_path = './outputs/' + sys.argv[1] + '/'
    col = (int(sys.argv[2]) if len(sys.argv) > 2
           else {'s': 15, 'h': 16, 'd': 16}[sys.argv[1][-1]])

    files = [join(file_path, f) for f in listdir(file_path)
             if isfile(join(file_path, f)) and not f.startswith('.')]

    files.sort(key=lambda fname: int(basename(fname)[4:-4]))

    _digs = '0123456789'
    avgs = []
    for f in files:
        with open(f) as file_:
            vals = []
            for line in file_:
                if not len(line.strip()):
                    break
                if line[0] not in _digs:
                    continue
                line = line.split(',')[col-1]
                vals.append(int(line))
            avgs.append(sum(vals)/len(vals))
    print avgs


if __name__ == '__main__':
    main()
