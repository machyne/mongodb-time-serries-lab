#! /bin/bash

# Sample
for i in 1 2 4 8 16 32 64 128; do
    echo Testing with $i threads...
    ./clean_db
    dstat -a --mongodb-op --mongodb-mem --mongodb-stat --mongodb-conn --output ./outputs/w_s_$i.csv > /dev/null &
    DSTATPID=$!
    ./bin/load 840 12 sample $i 240s localhost
    kill $DSTATPID
    echo && echo && echo
done

# Hour
for i in 1 2 4 8 16 32 64 128; do
    echo Testing with $i threads...
    ./clean_db
    dstat -a --mongodb-op --mongodb-mem --mongodb-stat --mongodb-conn --output ./outputs/w_h_$i.csv > /dev/null &
    DSTATPID=$!
    ./bin/load 840 12 hour $i 240s localhost
    kill $DSTATPID
    echo && echo && echo
done

# Day
for i in 1 2 4 8 16 32 64 128; do
    echo Testing with $i threads...
    ./clean_db
    dstat -a --mongodb-op --mongodb-mem --mongodb-stat --mongodb-conn --output ./outputs/w_d_$i.csv > /dev/null &
    DSTATPID=$!
    ./bin/load 840 12 day $i 240s localhost
    kill $DSTATPID
    echo && echo && echo
done
