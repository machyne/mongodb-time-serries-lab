#! /bin/bash

# Sample
for i in 1 2 4 8 16 32 64 128; do
    echo Testing with $i threads...
    ./clean_db

    mongo --eval "
        while(1){
            print(JSON.stringify(
                db.serverStatus({tcmalloc:1})));
            sleep(1000)}
    " > ./outputs/ssw_s_$i.log &
    MONGOPID=$!

    ./bin/load 840 12 sample $i 120s localhost
    kill $MONGOPID
    echo && echo && echo
done

# Hour
for i in 1 2 4 8 16 32 64 128; do
    echo Testing with $i threads...
    ./clean_db

    mongo --eval "
        while(1){
            print(JSON.stringify(
                db.serverStatus({tcmalloc:1})));
            sleep(1000)}
    " > ./outputs/ssw_h_$i.log &
    MONGOPID=$!

    ./bin/load 840 12 hour $i 120s localhost
    kill $MONGOPID
    echo && echo && echo
done

# Day
for i in 1 2 4 8 16 32 64 128; do
    echo Testing with $i threads...
    ./clean_db

    mongo --eval "
        while(1){
            print(JSON.stringify(
                db.serverStatus({tcmalloc:1})));
            sleep(1000)}
    " > ./outputs/ssw_d_$i.log &
    MONGOPID=$!

    ./bin/load 840 12 day $i 120s localhost
    kill $MONGOPID
    echo && echo && echo
done
